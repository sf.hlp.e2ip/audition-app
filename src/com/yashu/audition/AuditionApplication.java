package com.yashu.audition;

import java.util.Scanner;

public class AuditionApplication {

	private static Scanner input = new Scanner(System.in);
	private static String inputPerformerType;
	private static int totalPerformer, countPerformer, countDancer, countVocalists;

	public static void main(String[] args) {

		// Call method to get input for Performer Type
		inputPerformerType();

	}

	private static void inputPerformerType() {
		while (totalPerformer < 7) {
			System.out.println("Enter the type of performer");
			inputPerformerType = input.next();
			totalPerformer = pickAndDisplayPerformerType(inputPerformerType);
		}

	}

	private static int pickAndDisplayPerformerType(String performerType) {
		if (PerformerType.PERFORMER.getValue().equals(performerType)) {
			if (countPerformer < 4) {
				Performer.definePerformerObjects();
				Performer.perform(countPerformer);
				countPerformer++;
			} else {
				System.out.println("Performers are enough for Audition");
			}

		} else if (PerformerType.DANCER.getValue().equals(performerType)) {
			if (countDancer < 2) {
				Dancer.defineDancerObjects(countDancer);
				Dancer.perform(countDancer);
				countDancer++;

			} else {
				System.out.println("Dancers are enough for Audition");
			}

		} else if (PerformerType.VOCALIST.getValue().equals(performerType)) {
			if (countVocalists < 1) {
				Vocalist.defineVocalistObjects();
				Vocalist.perform(countVocalists);
				countVocalists++;

			} else {
				System.out.println("Vocalists are enough for Audition");
			}
		} else {
			System.out.println("Enter the Correct type of performer");

		}

		return totalPerformer = countPerformer + countDancer + countVocalists;

	}

}