package com.yashu.audition;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class Dancer {

	private static List<Dancer> dancers = new ArrayList<Dancer>();
	private String Style;
	private int randomNo;
	private int Id;

	public int getId() {
		return Id;
	}

	public String getStyle() {
		return Style;
	}

	public void setStyle(String style) {
		Style = style;
	}

	public void setRandomDancerId(int countId) {
		SecureRandom random = new SecureRandom();
		randomNo = random.nextInt();
		while (randomNo < 0) {

			randomNo = 1 + random.nextInt();
			Id = randomNo;

		}

		Id = 1 + randomNo;
	}

	// Define Dancer Objects
	public static List<Dancer> defineDancerObjects(int countDancer) {
		// Dancer[] dancerArray = new Dancer[2];
		for (int i = countDancer; i < 2; i++) {
			Dancer dancer = new Dancer();
			dancers.add(dancer);
			// }
			// for (Dancer dancer : dancers) {
			dancer.setRandomDancerId(countDancer);
			dancer.setStyle("tap");
		}
		return dancers;
	}

	public static void perform(int countDancer) {
		System.out.println(dancers.get(countDancer).getStyle() + "\t" + dancers.get(countDancer).getId() + " - Dancer");
	}

}
