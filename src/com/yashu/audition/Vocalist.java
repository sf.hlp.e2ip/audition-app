package com.yashu.audition;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Vocalist {

	private static List<Vocalist> vocalists = new ArrayList<Vocalist>();
	private static int volume;
	private static Scanner input = new Scanner(System.in);
	private String key;
	private int randomNo;
	private int Id;

	public int getId() {
		return Id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setRandomVocalistId() {
		SecureRandom random = new SecureRandom();
		randomNo = random.nextInt();
		while (randomNo < 0) {

			randomNo = 1 + random.nextInt();
			Id = randomNo;

		}

		Id = 1 + randomNo;
	}

	static List<Vocalist> defineVocalistObjects() {
		// Define Vocalist Object
		Vocalist[] vocalistArray = new Vocalist[1];
		for (int i = 0; i < vocalistArray.length; i++) {
			Vocalist vocalist = new Vocalist();
			vocalists.add(vocalist);
		}

		System.out.println("Enter a volume range between 1 - 10");
		volume = input.nextInt();
		System.out.println("Enter a key from A-Z");
		String key = input.next();

		for (Vocalist vocalist : vocalists) {
			vocalist.setRandomVocalistId();
			vocalist.setKey(key);

		}
		return vocalists;
	}

	public static void perform(int countVocalists) {
		System.out.println("I sing in the key of - " + vocalists.get(countVocalists).getKey() + "- at the volume "
				+ volume + "-" + vocalists.get(countVocalists).getId() + " - Vocalist");

	}
}
