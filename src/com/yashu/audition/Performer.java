/**
 * 
 */
package com.yashu.audition;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

/**
 * @author SFMOOCuser
 *
 */
public class Performer {

	private static List<Performer> performers = new ArrayList<Performer>();
	private int performerId;
	private int randomNo;

	public int getPerformerId() {
		return performerId;
	}

	public void setRandomPerformId() {
		SecureRandom random = new SecureRandom();
		randomNo = random.nextInt();
		while (randomNo < 0) {

			randomNo = 1 + random.nextInt();
			performerId = randomNo;

		}

		performerId = 1 + randomNo;
	}

	static List<Performer> definePerformerObjects() {
		// Define performer objects

		Performer[] performerArray = new Performer[4];
		for (int i = 0; i < performerArray.length; i++) {
			Performer performer = new Performer();
			performers.add(performer);
		}
		for (Performer performer : performers) {
			performer.setRandomPerformId();
		}
		return performers;

	}

	public static void perform(int countPerformer) {
		System.out.printf("%3d %s%n", performers.get(countPerformer).getPerformerId(), " - Performer");

	}

}
